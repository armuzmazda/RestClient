import model.User;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Administrador on 03/04/2016.
 */
public class MainClientPut {
    public static void main(String[] args) {

        User user = new User();
        user.setId(1);
        user.setUserName("alexandre");
        user.setPassword("123mudar");
        user.setAddress("Rua da rua");
        user.setEmail("mm@mm.com");

        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://localhost:8080/server");
        WebTarget resourceWebTarget = webTarget.path("rest");

        WebTarget deleteWeb = resourceWebTarget.path("user");
        Invocation.Builder deleteInvocationBuilder = deleteWeb.request();
        Response putResponse = deleteInvocationBuilder.put(Entity.entity(user, MediaType.APPLICATION_JSON_TYPE));

        System.out.println(putResponse.getStatus());
        System.out.println(putResponse.readEntity(String.class));
    }
}
