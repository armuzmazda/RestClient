import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.deploy.util.SessionState;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import model.User;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;

/**
 * Created by Administrador on 03/04/2016.
 */
public class MainClientGet {
    public static void main(String[] args){
        //Instancia o Client do javax.ws.rs.client.Client
        Client client = ClientBuilder.newClient();
        //Instancia o WebTarget passando o client.ENDEREÇO DO SERVIDOR REST / o contexto da applicacao
        WebTarget webTarget = client.target("http://localhost:8080/server");
        //Agora vamos montar a URL de pesquisa nessa sequencia
        //começamos com o recurso (Foi configurado no web.xml) do projeto
        WebTarget resourceWebTarget = webTarget.path("rest");
        //Passamos o caminho da requisição
        WebTarget pathdWebTarget = resourceWebTarget.path("user");
        //Dizemos o parâmetro de pesquisa
        WebTarget pathWebTargetQuery = pathdWebTarget.queryParam("id", 1);
        //Juntamos tudo isso com o Invocation.Builder(O Builder é uma Interface) e requisitamos uma resposta
        //do tipo JSON
        //URI final http://localhost:8080/server/rest/user/1
        Invocation.Builder invocationBuilder = pathWebTargetQuery.request(MediaType.APPLICATION_JSON_TYPE);
        //Solicitamos a resposta instanciando o Response do javax.ws.rs.core.Response
        Response response = invocationBuilder.get();
        //Instanciamos um novo Usuário para popularmos
        User user = response.readEntity(User.class);
        //Imprime a instancia de User no console
        System.out.println("Instancia do objeto User");
        System.out.println(user.getId());
        System.out.println(user.getUserName());
        System.out.println(user.getPassword());
        System.out.println(user.getAddress());
        System.out.println(user.getEmail());

        //Mapeando para Json
        System.out.println("Objeto user em JSON");
        XStream xStreamJSON = new XStream(new JettisonMappedXmlDriver());
        xStreamJSON.setMode(XStream.NO_REFERENCES);
        xStreamJSON.alias("user", User.class);
        System.out.println(xStreamJSON.toXML(user));

        //Mapeando para XML
        System.out.println("Objeto user em XML");
        XStream xStreamXML = new XStream();
        String xml = xStreamXML.toXML(user);
        System.out.println(xml);


    }
}
