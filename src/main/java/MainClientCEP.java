import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import model.CEP;
import model.User;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Scanner;

/**
 * Created by Administrador on 03/04/2016.
 */
public class MainClientCEP {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o cep:");
        String cepP = scanner.nextLine();

        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://viacep.com.br/");

        WebTarget resourceWebTarget = webTarget.path("ws");
        WebTarget cepWebTarget = resourceWebTarget.path(cepP);
        WebTarget mediaTypeWebTarget = cepWebTarget.path("json/");

        Invocation.Builder invocationBuilder = mediaTypeWebTarget.request(MediaType.APPLICATION_JSON_TYPE);

        Response response = invocationBuilder.get();

        CEP cep = response.readEntity(CEP.class);

        System.out.println("Rua: " + cep.getLogradouro());
        System.out.println("Complemento: " + cep.getComplemento());
        System.out.println("Bairro: " + cep.getBairro());
        System.out.println("Cidade: " + cep.getLocalidade());
        System.out.println("Estado: " + cep.getUf());
        System.out.println("Cod IBGE: " + cep.getIbge());

        //Mapeando para Json
        System.out.println("Objeto user em JSON");
        XStream xStreamJSON = new XStream(new JettisonMappedXmlDriver());
        xStreamJSON.setMode(XStream.NO_REFERENCES);
        xStreamJSON.alias("user", User.class);
        System.out.println(xStreamJSON.toXML(cep));

        //Mapeando para XML
        System.out.println("Objeto user em XML");
        XStream xStreamXML = new XStream();
        String xml = xStreamXML.toXML(cep);
        System.out.println(xml);
    }
}
